package com.ap.webnotes.model;

import java.util.List;

public class IDs {

    private List<Integer> listIds;

    public List<Integer> getListIds() {
        return listIds;
    }

    public IDs setListIds(List<Integer> listIds) {
        this.listIds = listIds;
        return this;
    }
}
