package com.ap.webnotes.dto.users;

public class UserDto {
    private Integer id;
    private String user;
    private String passwd;

    public String getUser() {
        return user;
    }

    public UserDto setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPasswd() {
        return passwd;
    }

    public UserDto setPasswd(String passwd) {
        this.passwd = passwd;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public UserDto setId(Integer id) {
        this.id = id;
        return this;
    }
}
