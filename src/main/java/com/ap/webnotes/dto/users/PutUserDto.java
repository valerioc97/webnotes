package com.ap.webnotes.dto.users;

public class PutUserDto {

    private String user;

    public String getUser() {
        return user;
    }

    public PutUserDto setUser(String user) {
        this.user = user;
        return this;
    }
}
