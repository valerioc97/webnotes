package com.ap.webnotes.utils;

import com.ap.webnotes.dto.notes.NotaDto;
import com.ap.webnotes.dto.users.UserDto;
import com.ap.webnotes.model.notes.Nota;
import com.ap.webnotes.model.users.Users;

import java.util.List;

public class Utility {

    /**
     * method that allow to verify if an user exist or none
     *
     * @param checkUser     user from external source
     * @param usersToInsert user insert from dto
     * @return true if user exist or false if user not exist
     */

    //TODO fix bug
    public static boolean checkUserExsistence(List<Users> checkUser, UserDto usersToInsert, Users putUserDto) {

        if (usersToInsert != null && !checkUser.isEmpty()) {
            Boolean flg = false;
            for (Users us : checkUser) {
                if (us.getUser().equals(usersToInsert.getUser())) {
                    return true;
                } else {
                    return false;
                }
            }
            return flg;
        } else if (putUserDto != null && !checkUser.isEmpty()) {
            for (Users us : checkUser) {
                if (us.getUser().equals(putUserDto.getUser())) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        return false;
    }


    /**
     * method that allow to verify if a note is already exist or none
     *
     * @param note         list of notes from external source
     * @param noteToInsert note insert from dto
     * @return true if note exist or false if note not exist
     */
    public static boolean checkNotaExisistence(List<Nota> note, NotaDto noteToInsert) {
        return !note.isEmpty() && note.stream()
                .anyMatch(check ->
                        check != null && check.getTitolo() != null && check.getTitolo().contains(noteToInsert.getTitolo()));
    }
}
