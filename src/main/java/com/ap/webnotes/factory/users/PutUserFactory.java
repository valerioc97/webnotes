package com.ap.webnotes.factory.users;

import com.ap.webnotes.dto.users.UserDto;
import com.ap.webnotes.model.users.Users;
import org.springframework.stereotype.Service;

@Service
public class PutUserFactory {

    public Users dtoToModel(UserDto dto, Integer id){
        return new Users()
                .setId(id)
                .setUser(dto.getUser());
    }
}
